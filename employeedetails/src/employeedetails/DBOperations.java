/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employeedetails;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

/**
 *
 * @author kemal
 */
public class DBOperations {
    
    String username="root";
    String password="";
    String url="jdbc:mysql://localhost:3306/employee";
    Connection con=null;
    PreparedStatement pst=null;
    void addEmployee(EmployeeDetail em){
        try{
   con=(Connection)DriverManager.getConnection(url, username, password);
   String query="insert into employeedetails values(?,?,?,?,?,?,?,?)";
   pst=(PreparedStatement)con.prepareStatement(query);
   pst.setInt(1, em.getRegID());
   pst.setString(2,em.getFirstname());
   pst.setString(3,em.getLastname());
   pst.setInt(4,em.getAge());
   pst.setString(5, em.getCountry());
   pst.setString(6,em.getEmail());
   pst.setString(7, em.getUsername());
   pst.setString(8, em.getPassword());
   pst.executeUpdate();
        }
        catch(Exception e)
        {
            System.out.println(e);
        }
    
    }
}
// this is a comment for testing